package no.ntnu.mappe2;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ImportExportCSVTest {
    private final PatientRegister register = new PatientRegister();
    private final Patient patientA = new Patient("Spike","Spiegel", "Jet Black", "20440626877", "");
    private final Patient patientB = new Patient("Roy","Mustang", "Riza Hawkeye", "18850925918", "");
    private final Patient patientC = new Patient("Killua","Zoldyck", "Gon Freecss", "19990707908", "");
    private final Patient patientD = new Patient("Yui","Hirasawa", "Sawako Yamanaka", "19911127784", "");

    private final File file = new File("file.test");

    @AfterAll
    public void cleanUp() {
        file.delete();
    }

    @Test
    public void testImportPatientCSV() throws IOException {
        ArrayList<Patient> expected = new ArrayList<>();
        expected.add(patientC);
        expected.add(patientD);

        ImportExportCSV importer = new ImportExportCSV();
        Files.writeString(Path.of(file.getAbsolutePath()), "firstName;lastName;practitioner;socialSecurityNumber;diagnosis\nKillua;Zoldyck;Gon Freecss;19990707908;\nYui;Hirasawa;Sawako Yamanaka;19911127784;\n");

        ArrayList<Patient> actual = importer.importPatientRegister(file);

        Assertions.assertTrue(actual.toString().contentEquals(expected.toString()));
    }


    @Test
    public void testExportPatientCSV() throws IOException {
        register.addPatient(patientA);
        register.addPatient(patientB);

        ImportExportCSV exporter = new ImportExportCSV();
        exporter.exportPatientRegister(file, register);

        String expected = "firstName;lastName;practitioner;socialSecurityNumber;diagnosis\nSpike;Spiegel;Jet Black;20440626877;\nRoy;Mustang;Riza Hawkeye;18850925918;\n";
        String actual = Files.readString(Path.of(file.getAbsolutePath()));

        Assertions.assertEquals(expected , actual);
    }
}
