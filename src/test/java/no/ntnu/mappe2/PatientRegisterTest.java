package no.ntnu.mappe2;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Assertions;

public class PatientRegisterTest {
    private final PatientRegister register = new PatientRegister();
    private final Patient patientA = new Patient("Spike","Spiegel", "Jet Black", "20440626877", "");
    private final Patient patientB = new Patient("Roy","Mustang", "Riza Hawkeye", "18850925918", "");
    private final Patient patientC = new Patient("Killua","Zoldyck", "Gon Freecss", "19990707908", "");
    private final Patient patientD = new Patient("Yui","Hirasawa", "Sawako Yamanaka", "19911127784", "");

    private void fillRegister() {
        register.addPatient(patientA);
        register.addPatient(patientB);
        register.addPatient(patientC);
    }

    @Test
    public void testAddPatient_True() {
        Assertions.assertTrue(register.addPatient(patientA));
        Assertions.assertEquals(1, register.getPatients().size());
    }

    @Nested
    public class _testRemovePatient {

        @Test
        public void true_PatientExists() {
            fillRegister();
            Assertions.assertTrue(register.removePatient(patientB));
            Assertions.assertEquals(2, register.getPatients().size());
        }

        @Test
        public void false_NotInRegister() {
            fillRegister();
            Assertions.assertFalse(register.removePatient(patientD));
            Assertions.assertEquals(3, register.getPatients().size());
        }
    }
}
