package no.ntnu.mappe2;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.IOException;

public class App extends Application {
    // ▼ Defaults
    public static String version = "1.0";
    public static String repository = "https://gitlab.stud.idi.ntnu.no/matssg/idatt2001-mappe-2";
    // ▲ Defaults

    public static PatientRegister register = new PatientRegister();

    private static Scene scene;
    public static Stage appStage;

    @Override
    public void start(Stage stage) throws Exception {
        stage.setTitle("Patient Registry " + version);
        stage.getIcons().add(new Image(getClass().getResourceAsStream("/images/logo.png")));
        scene = new Scene(loadFXML("main"));
        stage.setScene(scene);
        appStage = stage;
        stage.show();

        stage.setOnCloseRequest(event -> {
            exitAlert();
            event.consume();
        });
    }

    public static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("/fxml/" + fxml + ".fxml"));
        return fxmlLoader.load();
    }

    /**
    * Creates and displays a new {@code Alert}. Exits the program if {@code YES} is selected.
    */
    public static void exitAlert() {
        Alert alert = new Alert(Alert.AlertType.NONE, "Are you sure you want to exit the application?", ButtonType.YES, ButtonType.CANCEL);
        alert.getDialogPane().getStylesheets().add(App.class.getResource("/style/light-theme.css").toExternalForm());
        ((Stage) alert.getDialogPane().getScene().getWindow()).getIcons().add(new Image(App.class.getResourceAsStream("/images/logo.png")));
        alert.setTitle("Warning");
        alert.setResizable(false);
        alert.showAndWait();

        if (alert.getResult() == ButtonType.YES) {System.exit(0);}
    }

    public static void main(String[] args) {
        launch();
    }
}