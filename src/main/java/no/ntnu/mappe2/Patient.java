package no.ntnu.mappe2;

public class Patient {
    private String socialSecurityNumber;
    private String firstName;
    private String lastName;
    private String diagnosis;
    private String practitioner;

    public Patient(String firstName, String lastName, String practitioner, String socialSecurityNumber, String diagnosis) {
        this.firstName = firstName;
        this.lastName = lastName;
        if (socialSecurityNumber.length() != 11) {this.socialSecurityNumber = "";} 
        else {this.socialSecurityNumber = socialSecurityNumber;}
        this.practitioner = practitioner;
        this.diagnosis = diagnosis;
    }

    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }
    public String getFirstName() {
        return firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public String getDiagnosis() {
        return diagnosis;
    }
    public String getPractitioner() {
        return practitioner;
    }

    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }
    public void setPractitioner(String practitioner) {
        this.practitioner = practitioner;
    }

    @Override
    public String toString() {
        return (getSocialSecurityNumber() + 
                "\n\tName: " + getLastName() + ", " + getFirstName() +
                "\n\tDiagnosis: " + getDiagnosis() + 
                "\n\tPractitioner: " + getPractitioner());
    }
}
