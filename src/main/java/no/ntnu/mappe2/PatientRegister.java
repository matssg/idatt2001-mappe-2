package no.ntnu.mappe2;

import java.util.ArrayList;

public class PatientRegister {
    private ArrayList<Patient> patients;

    public PatientRegister() {
        this.patients = new ArrayList<Patient>();
    }

    public ArrayList<Patient> getPatients() {
        return patients;
    }

    /**
     * Add {@link Patient} to the patient register.
     * @param patient
     * @return {@code true} if successfully added;
     *         {@code false} if not added.
     */
    public boolean addPatient(Patient patient) {
        boolean result = false;

        if (!patients.contains(patient)) {
            patients.add(patient);
            result = true;
        }
        return result;
    }

    /**
     * Removes given {@link Patient} from register
     * @param patient
     * @return {@code true} if successfully removed;
     *         {@code false} if not removed.
     */
    public boolean removePatient(Patient patient) {
        return patients.remove(patient);
    }
    
    @Override
    public String toString() {
        String string = "Patients:";
        for (Patient patient : patients) {
            string += "\n\t" + patient.getLastName() + ", " + patient.getFirstName() + " (" + patient.getSocialSecurityNumber() + ")";
        }
        return string;
    }
}
