package no.ntnu.mappe2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

/**
 * Importing and exporting {@link PatientRegister}s as Comma Separated Values (.csv) files
 */
public class ImportExportCSV {
    private final String FIRST_NAME = "firstName";
    private final String LAST_NAME = "lastName";
    private final String PRACTITIONER = "practitioner";
    private final String SOCIAL_SECURITY_NUMBER = "socialSecurityNumber";
    private final String DIAGNOSIS = "diagnosis";

    private final String LINE_SEPARATOR = "\n";
    private final String DELIMITER = ";";

    /**
     * Reads data from .csv file and creates new {@link Patient}s.
     * @param file to read data from
     * @return list of {@link Patient}s
     */
    public ArrayList<Patient> importPatientRegister(File file) {
        ArrayList<Patient> patients = new ArrayList<>();

        try {
            BufferedReader csvReader = new BufferedReader(new FileReader(file));

            // Split header to see what information is present and in which order
            String header = csvReader.readLine();
            ArrayList<String> headerList = new ArrayList<>(Arrays.asList(header.split(DELIMITER)));

            int firstNameIndex = headerList.indexOf(FIRST_NAME);
            int lastNameIndex = headerList.indexOf(LAST_NAME);
            int practitionerIndex = headerList.indexOf(PRACTITIONER);
            int ssnIndex = headerList.indexOf(SOCIAL_SECURITY_NUMBER);
            int diagnosisIndex = headerList.indexOf(DIAGNOSIS);

            // Make new patient objects for each line in file
            String line;
            while ((line = csvReader.readLine()) != null) {
                ArrayList<String> data = new ArrayList<>(Arrays.asList(line.split(DELIMITER)));

                    String firstName = "";
                    String lastName = "";
                    String practitioner = "";
                    String ssn = "";
                    String diagnosis = "";

                    if (!(firstNameIndex == -1 || firstNameIndex >= data.size()))       {firstName = data.get(firstNameIndex);}
                    if (!(lastNameIndex == -1 || lastNameIndex >= data.size()))         {lastName = data.get(lastNameIndex);}
                    if (!(practitionerIndex == -1 || practitionerIndex >= data.size())) {practitioner = data.get(practitionerIndex);}
                    if (!(ssnIndex == -1 || ssnIndex >= data.size()))                   {ssn = data.get(ssnIndex);}
                    if (!(diagnosisIndex == -1 || diagnosisIndex >= data.size()))       {diagnosis = data.get(diagnosisIndex);}

                    patients.add(new Patient(firstName, lastName, practitioner, ssn, diagnosis));
                }
            csvReader.close();
        } 
        
        catch (Exception e) {
            System.err.println("[ERROR] " + e.getMessage());
        }
        return patients;
    }

    /**
     * Writes {@link Patient}s to file with structure firstName;lastName;practitioner;ssn;diagnosis
     * @param file to write to
     * @param patients {@link PatientRegister} to export
     * @return written {@code File}
     */
    public boolean exportPatientRegister(File file, PatientRegister patients) {

        try {
    		FileWriter fileWriter = new FileWriter(file);
    		
    		// Add header
    		fileWriter.append(FIRST_NAME + DELIMITER + LAST_NAME + DELIMITER + PRACTITIONER + DELIMITER + SOCIAL_SECURITY_NUMBER + DELIMITER + DIAGNOSIS);
    		fileWriter.append(LINE_SEPARATOR);
    		
    		// Iterate through register
    		Iterator<Patient> it = patients.getPatients().iterator();
    		while (it.hasNext()) {
    			Patient patient = (Patient) it.next();

                fileWriter.append(patient.getFirstName());
    			fileWriter.append(DELIMITER);

                fileWriter.append(patient.getLastName());
    			fileWriter.append(DELIMITER);

                fileWriter.append(patient.getPractitioner());
    			fileWriter.append(DELIMITER);

                fileWriter.append(patient.getSocialSecurityNumber());
                fileWriter.append(DELIMITER);

                fileWriter.append(patient.getDiagnosis());
    			fileWriter.append(LINE_SEPARATOR);
    		}
            fileWriter.close();
            return true;
    	} 
        
        catch (Exception e) {
            System.err.println("[ERROR] " + e.getMessage());
        }
        return false;
    }
}
