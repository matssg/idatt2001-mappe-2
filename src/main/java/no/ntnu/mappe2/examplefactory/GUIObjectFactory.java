package no.ntnu.mappe2.examplefactory;

public class GUIObjectFactory {
    
    public GUIObject getObject(String object) {
        if (object == null) {return null;}

        if (object.equalsIgnoreCase("MENUBAR")) {
            return new MenuBarObject();
        } // Add more objects
        
        return null;
    }
}
