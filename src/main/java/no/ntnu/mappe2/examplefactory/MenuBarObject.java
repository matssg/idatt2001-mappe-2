package no.ntnu.mappe2.examplefactory;

import javafx.scene.Node;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import no.ntnu.mappe2.App;
import no.ntnu.mappe2.controller.MainController;

public class MenuBarObject implements GUIObject {
    private MainController mainController = new MainController();
    
    @Override
    public Node create() {
        // File
        Menu menuFile = new Menu("File");

        MenuItem importFile = new MenuItem("Import from .csv");
        MenuItem exportFile = new MenuItem("Export to .csv");
        SeparatorMenuItem separator = new SeparatorMenuItem();
        MenuItem exit = new MenuItem("Exit");

        importFile.setOnAction(event -> mainController.handleImportButtonAction());
        exportFile.setOnAction(event -> mainController.handleExportButtonAction());
        exit.setOnAction(event -> App.exitAlert());

        menuFile.getItems().addAll(importFile, exportFile, separator, exit);

        // Edit
        Menu menuEdit = new Menu("Edit");
        MenuItem add = new MenuItem("Add new Patient...");
        MenuItem edit = new MenuItem("Edit selected Patient...");
        MenuItem remove = new MenuItem("Remove selected Patient...");

        add.setOnAction(event -> mainController.handleAddPatientButtonAction());
        edit.setOnAction(event -> mainController.handleEditPatientButtonAction());
        remove.setOnAction(event -> mainController.handleRemovePatientButtonAction());

        add.setAccelerator(new KeyCodeCombination(KeyCode.A));
        edit.setAccelerator(new KeyCodeCombination(KeyCode.E));
        remove.setAccelerator(new KeyCodeCombination(KeyCode.DELETE));

        menuEdit.getItems().addAll(add, edit, remove);

        // Help
        Menu menuHelp = new Menu("Help");
        MenuItem about = new MenuItem("About");

        about.setOnAction(event -> mainController.handleAboutButtonAction());

        menuHelp.getItems().add(about);

        // Create MenuBar
        return new MenuBar(menuFile, menuEdit, menuHelp);
    }
}
