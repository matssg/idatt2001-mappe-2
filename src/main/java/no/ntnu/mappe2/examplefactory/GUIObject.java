package no.ntnu.mappe2.examplefactory;

import javafx.scene.Node;

public interface GUIObject {
    public Node create();
}
