package no.ntnu.mappe2.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import no.ntnu.mappe2.*;

public class ItemController {
    // ▼ Labels
    @FXML Label firstNameLabel;
    @FXML Label lastNameLabel;
    @FXML Label ssnLabel;
    // ▼ Textfields
    @FXML TextField firstNameText;
    @FXML TextField lastNameText;
    @FXML TextField ssnText;
    @FXML TextField practitionerText;
    @FXML TextField diagnosisText;
    // ▼ Buttons
    @FXML Button saveButton;

    public static boolean editing;
    public static Patient selected = null;

    /**
     * Called to initialize a controller after its root element has been completely processed.
     */
    public void initialize() {
        if (editing) {
            saveButton.setText("Save");
            saveButton.setOnAction((ActionEvent event) -> {handleSaveButtonAction(event);});

            if (!selected.getFirstName().isEmpty()) {firstNameText.setText(selected.getFirstName());}
            if (!selected.getLastName().isEmpty()) {lastNameText.setText(selected.getLastName());}
            if (isValidSSN(selected.getSocialSecurityNumber())) {ssnText.setText(selected.getSocialSecurityNumber());}
            if (!selected.getPractitioner().isEmpty()) {practitionerText.setText(selected.getPractitioner());}
            if (!selected.getDiagnosis().isEmpty()) {diagnosisText.setText(selected.getDiagnosis());}
        } else {
            saveButton.setText("Add");
            saveButton.setOnAction((ActionEvent event) -> {handleAddButtonAction(event);});
        }
    }

    /**
     * Sets color of all required labels to {@code Color.BLACK}
     */
    private void resetLabels() {
        firstNameLabel.setTextFill(Color.BLACK);
        lastNameLabel.setTextFill(Color.BLACK);
        ssnLabel.setTextFill(Color.BLACK);
    }

    /**
     * Checks if Social Security Number is valid.
     * <p>
     * A valid Social Security Number consists of 11 digits.
     * @param ssn
     * @return {@code true} if valid;
     *         {@code false} if not
     */
    public boolean isValidSSN(String ssn) {
        if (ssn.isEmpty() || ssn.length() != 11) {
            return false;
        } else {
            for (int i = 0; i < ssn.length(); i++) {
                if (!Character.isDigit(ssn.charAt(i)))
                    return false;
            }
            return true;
        }
    }

    public void handleCancelButtonAction(ActionEvent event) {
        ((Stage) ((Button) event.getSource()).getScene().getWindow()).close();
    }

    public void handleAddButtonAction(ActionEvent event) {
        String firstName = firstNameText.getText().trim();
        String lastName = lastNameText.getText().trim();
        String ssn = ssnText.getText().trim();
        String practitioner = practitionerText.getText().trim();
        String diagnosis = diagnosisText.getText().trim();

        resetLabels();

        // Check if all required fields are filled in correctly
        if (firstName.isEmpty() || lastName.isEmpty() || !isValidSSN(ssn)) {
            if (firstName.isEmpty()) {firstNameLabel.setTextFill(Color.RED);}
            if (lastName.isEmpty()) {lastNameLabel.setTextFill(Color.RED);}
            if (!isValidSSN(ssn)) {ssnLabel.setTextFill(Color.RED);}
        }

        // If all fields are filled in -> create new Patient object
        else {
            App.register.addPatient(new Patient(firstName, lastName, practitioner, ssn, diagnosis));
            ((Stage) ((Button) event.getSource()).getScene().getWindow()).close();
        }
    }

    public void handleSaveButtonAction(ActionEvent event) {
        String firstName = firstNameText.getText().trim();
        String lastName = lastNameText.getText().trim();
        String ssn = ssnText.getText().trim();
        String practitioner = practitionerText.getText().trim();
        String diagnosis = diagnosisText.getText().trim();

        resetLabels();

        // Check if all required fields are filled in correctly
        if (firstName.isEmpty() || lastName.isEmpty() || !isValidSSN(ssn)) {
            if (firstName.isEmpty()) {firstNameLabel.setTextFill(Color.RED);}
            if (lastName.isEmpty()) {lastNameLabel.setTextFill(Color.RED);}
            if (!isValidSSN(ssn)) {ssnLabel.setTextFill(Color.RED);}
        } 

        // If all fields are filled in -> edit Patient object
        else {
            selected.setFirstName(firstName);
            selected.setLastName(lastName);
            selected.setSocialSecurityNumber(ssn);
            selected.setPractitioner(practitioner);
            if (!(diagnosis == null)) {selected.setDiagnosis(diagnosis);}
            ((Stage) ((Button) event.getSource()).getScene().getWindow()).close();
        }
    }
}
