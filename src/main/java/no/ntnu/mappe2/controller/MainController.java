package no.ntnu.mappe2.controller;

import no.ntnu.mappe2.*;

import javafx.fxml.FXML;

import java.io.File;
import java.io.IOException;

import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.event.ActionEvent;

import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class MainController {
    // ▼ Menubar
    @FXML MenuItem menuExit;
    // ▼ Main table
    @FXML TableView<Patient> tableView;
    // ▼ Column
    @FXML TableColumn<Patient,String> lastNameColumn;
    @FXML TableColumn<Patient,String> firstNameColumn;
    @FXML TableColumn<Patient,String> ssnColumn;
    @FXML TableColumn<Patient,String> practitionerColumn;
    @FXML TableColumn<Patient,String> diagnosisColumn;

    /**
     * Called to initialize a controller after its root element has been completely processed.
     */
    public void initialize() {
        menuExit.setOnAction((ActionEvent t) -> {App.exitAlert();});

        // Connect columns to values
        lastNameColumn.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().getLastName()));
        firstNameColumn.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().getFirstName()));
        ssnColumn.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().getSocialSecurityNumber()));
        practitionerColumn.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().getPractitioner()));
        diagnosisColumn.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().getDiagnosis()));
    }

    /**
     * Clears, refills and sorts {@code tableView}.
     */
    public void fillTable() {
        tableView.getItems().clear();
        for (Patient patient : App.register.getPatients()) {
            tableView.getItems().add(patient);
        }
        tableView.sort();
    }

    /**
     * Creates and displays a new {@code Alert}.
     */
    private void openAlertSelectPatient() {
        Alert alert = new Alert(Alert.AlertType.NONE, "Please select patient", ButtonType.OK);
        alert.getDialogPane().getStylesheets().add(getClass().getResource("/style/light-theme.css").toExternalForm());
        ((Stage) alert.getDialogPane().getScene().getWindow()).getIcons().add(new Image(getClass().getResourceAsStream("/images/logo.png")));
        alert.setTitle("Warning");
        alert.setResizable(false);
        alert.showAndWait();
    }

    public void handleAddPatientButtonAction() {
        ItemController.editing = false;
        try {
            Stage stage = new Stage();
            stage.setTitle("Add");
            stage.getIcons().add(new Image(getClass().getResourceAsStream("/images/logo.png")));
            stage.setResizable(false);
            stage.initStyle(StageStyle.UNIFIED);
            Scene scene = new Scene(App.loadFXML("item"));
            stage.setScene(scene);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
        }
        fillTable();
    }

    public void handleEditPatientButtonAction() {
        ItemController.editing = true;
        if (tableView.getSelectionModel().getSelectedItem() == null) {
            openAlertSelectPatient();
        } else {
            ItemController.selected = tableView.getSelectionModel().getSelectedItem();
            try {
                Stage stage = new Stage();
                stage.setTitle("Edit");
                stage.getIcons().add(new Image(getClass().getResourceAsStream("/images/logo.png")));
                stage.setResizable(false);
                stage.initStyle(StageStyle.UNIFIED);
                Scene scene = new Scene(App.loadFXML("item"));
                stage.setScene(scene);
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.showAndWait();
            } catch (IOException e) {
                e.printStackTrace();
            }
            fillTable();
        }
    }

    public void handleRemovePatientButtonAction() {
        if (tableView.getSelectionModel().getSelectedItem() == null) {
            openAlertSelectPatient();
        } else {
            Alert alert = new Alert(Alert.AlertType.NONE, "Are you sure you want to delete this patient?", ButtonType.YES, ButtonType.NO);
            alert.getDialogPane().getStylesheets().add(getClass().getResource("/style/light-theme.css").toExternalForm());
            ((Stage) alert.getDialogPane().getScene().getWindow()).getIcons().add(new Image(getClass().getResourceAsStream("/images/logo.png")));
            alert.setTitle("Delete");
            alert.setResizable(false);
            alert.showAndWait();

            if (alert.getResult() == ButtonType.YES) {
                App.register.removePatient(tableView.getSelectionModel().getSelectedItem());
                fillTable();
            }
        }
    }

    public void handleAboutButtonAction() {
        try {
            Stage stage = new Stage();
            stage.setTitle("About");
            stage.getIcons().add(new Image(getClass().getResourceAsStream("/images/logo.png")));
            stage.setResizable(false);
            stage.initStyle(StageStyle.UNIFIED);
            Scene scene = new Scene(App.loadFXML("about"));
            stage.setScene(scene);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void handleImportButtonAction() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open CSV");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("CSV files (*.csv)", "*.csv"));
        fileChooser.setInitialDirectory(
            new File(System.getProperty("user.dir"))
        );
        File selected = fileChooser.showOpenDialog(App.appStage);

        if (selected != null) {
            ImportExportCSV importer = new ImportExportCSV();

            App.register.getPatients().clear();
            for (Patient patient : importer.importPatientRegister(selected)) {
                App.register.addPatient(patient);
            };
            fillTable();
        }
    }

    public void handleExportButtonAction() {
		ImportExportCSV exporter = new ImportExportCSV();

        // Create new FileChooser to choose .csv file
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save CSV");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("CSV files (*.csv)", "*.csv"));
        fileChooser.setInitialDirectory(
            new File(System.getProperty("user.dir"))
        );

        File dest = fileChooser.showSaveDialog(App.appStage);

        if (dest != null && App.register !=null) {
            exporter.exportPatientRegister(dest, App.register);
        }
    }
}
