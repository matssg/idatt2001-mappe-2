package no.ntnu.mappe2.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import no.ntnu.mappe2.App;

public class AboutController {
    @FXML Label version;

    public void initialize() {
        version.setText(App.version);
    }

    /**
     * Opens repository URI in a new browser window or tab.
     */
    public void openRepository() {
        (new App()).getHostServices().showDocument(App.repository);
    }
}
