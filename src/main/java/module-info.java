module no.ntnu.mappe2 {
    requires transitive javafx.controls;
    requires javafx.fxml;

    opens no.ntnu.mappe2.controller to javafx.fxml;
    exports no.ntnu.mappe2;
}