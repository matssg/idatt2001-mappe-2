# IDATx2001 Programmering 2 - Mappe 2

### Problemstilling

I denne oppgaven skal du utvikle en pasient‐register applikasjon for et sykehus.Applikasjonen skal være komplett med et grafisk brukergrensesnitt (GUI) og med lagring av pasienter til fil (persistens). Oppgaven baserer seg på problemstillingen fra _Mappe 1_.
